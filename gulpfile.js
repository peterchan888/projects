/* File: gulpfile.js */

// grab our packages
var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    server = require('gulp-express');

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('./server/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('./server/**/*.js', ['jshint']);
});


gulp.task('server', function () {
    // Start the server at the beginning of the task 
    server.run(['server.js']);
 
    // Restart the server when file changes 
    //gulp.watch(['app/**/*.html'], server.notify);
    //gulp.watch(['app/styles/**/*.scss'], ['styles:scss']);
    //gulp.watch(['{.tmp,app}/styles/**/*.css'], ['styles:css', server.notify]); 
    //Event object won't pass down to gulp.watch's callback if there's more than one of them. 
    //So the correct way to use server.notify is as following: 
    //gulp.watch(['app/images/**/*'], server.notify);
    
    gulp.watch(['server.js', './server/**/*.js'], [server.run]);
});