angular.module('user.ctrl', ['user.services', 'angular-jwt'])
  .controller('SigninCtrl', ['$scope', '$state', 'principal', function($scope, $state, principal) {
      $scope.signin = function() {
        
        // here, we fake authenticating and give a fake user
        principal.authenticate({
          name: 'Test User',
          roles: ['user']
        });
        
        if ($scope.returnToState) $state.go($scope.returnToState.name, $scope.returnToStateParams);
        else $state.go('home');
      };
    }
  ])
  .controller('HomeCtrl', ['$scope', '$state', 'principal',
    function($scope, $state, principal) {
      $scope.signout = function() {
        principal.authenticate(null);
        $state.go('login');
      };
    }
  ])
  .controller('LocalSignUpCtrl', ['$scope', '$state', 'principal', '$http', 'jwtHelper',
    function($scope, $state, principal, $http, jwtHelper) {
      $scope.signup = function() {
        //principal.authenticate(null);
        //$state.go('login');

        //$scope.formData = {}; // clear the form so our user is ready to enter another
 

        $http.post('/signup', $scope.formData)
                .success(function(data) {
                    $scope.products = data;
                    
                    var decodedToken = jwtHelper.decodeToken(data);
                    console.log(decodedToken);
                })
                .error(function(data) {
                    console.log('Error: ' + data);
                });

      };
    }
  ])
