(function(angular) {
  angular.module('user.services', [])
    .factory('principal', ['$q', '$http', '$timeout',
      function($q, $http, $timeout) {
        var _identity = undefined,
          _authenticated = false;

        return {
          isIdentityResolved: function() {
            return angular.isDefined(_identity);
          },

          isAuthenticated: function() {
            return _authenticated;
          },

          isInRole: function(role) {
            if (!_authenticated || !_identity.roles) return false;

            return _identity.roles.indexOf(role) != -1;
          },

          isInAnyRole: function(roles) {
            if (!_authenticated || !_identity.roles) return false;

            for (var i = 0; i < roles.length; i++) {
              if (this.isInRole(roles[i])) return true;
            }

            return false;
          },

          authenticate: function(identity) {
            _identity = identity;
            _authenticated = identity !== null;

            if (identity) sessionStorage.setItem("ecom.identity", angular.toJson(identity));
            else sessionStorage.removeItem("ecom.identity");
          },

          identity: function(force) {
            var deferred = $q.defer();

            if (force === true) _identity = undefined;

            // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
            if (angular.isDefined(_identity)) {
              deferred.resolve(_identity);

              return deferred.promise;
            }

            var self = this;

            $http.get('/api/account')
              .success(function(data) {
                self.authenticate(data);
                deferred.resolve(_identity);
              })
              .error(function() {
                self.authenticate(null);
                deferred.resolve(_identity);
              });

            return deferred.promise;
          }
        };
      }
    ]);
})(window.angular);