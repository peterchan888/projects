var authRoutes = angular.module('authRoutes', ['ui.router', 'user.ctrl', 'auth.services']);


authRoutes.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/');

/*
  $stateProvider  
    .state('anon', {
      url : '/',
      abstract: true,
      template: '<ui-view/>',
      data: {
        //access: AccessLevels.anon
      }
    })
    .state("anon.home", {
      url: "",
      templateUrl: "/views/auth-landing.html"
    })
    .state('anon.login', {
      url: '/login',
      templateUrl: '/views/login.html',
      controller: 'LoginController'
    })
    .state('anon.signup', {
      url: '/signup',
      templateUrl: '/views/signup.html',
      controller: 'RegisterController'
    });
*/

    $stateProvider.state('site', {
      'abstract': true,
      resolve: {
        authorize: ['authorization',
          function(authorization) {
            return authorization.authorize();
          }
        ]
      }
    }).state('home', {
      parent: 'site',
      url: '/',
      data: {
        roles: []
      },
      views: {
        'content@': {
          templateUrl: "/views/auth-landing.html",
          controller: 'HomeCtrl'
        }
      }
    }).state('login', {
      parent: 'site',
      url: '/login',
      data: {
        roles: []
      },
      views: {
        'content@': {
          templateUrl: '/views/login.html',
          controller: 'SigninCtrl'
        }
      }  
    })
    .state('signup', {
      parent: 'site',
      url: '/signup',
      data: {
        roles: []
      },
      views: {
        'content@': {
          templateUrl: '/views/signup.html',
          controller: 'LocalSignUpCtrl'
        }
      }
    })

    .state('restricted', {
      parent: 'site',
      url: '/restricted',
      data: {
        roles: ['admin']
      },
      views: {
        'content@': {
          templateUrl: 'restricted.html'
        }
      }
    }).state('accessdenied', {
      parent: 'site',
      url: '/denied',
      data: {
        roles: []
      },
      views: {
        'content@': {
          templateUrl: 'denied.html'
        }
      }
    });
  



/*
  $stateProvider
    .state('user', {
      abstract: true,
      template: '<ui-view/>',
      data: {
        //access: AccessLevels.user
      }
    })
    .state('user.messages', {
      url: '/messages',
      templateUrl: 'user/messages.html',
      controller: 'MessagesController'
    });
*/
  });