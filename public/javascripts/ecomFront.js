/// <reference path="../../typings/angularjs/angular.d.ts"/>

var ecomFront = angular.module('ecomFront', ['authRoutes', 'ecomFront.products', 'ConsoleLogger']);

ecomFront
  .run(['$rootScope', '$state', '$stateParams', 'authorization', 'principal', 'PrintToConsole',
    function($rootScope, $state, $stateParams, authorization, principal, PrintToConsole) {

		PrintToConsole.active = false;

      $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {

        $rootScope.toState = toState;
        $rootScope.toStateParams = toStateParams;

        if (principal.isIdentityResolved()) authorization.authorize();
      });
    }
 ]);    

