
var url = function() {
	// default value for localhost or use openshift if
	// available
	result = 'mongodb://localhost/ecom';
	if (process.env.OPENSHIFT_MONGODB_DB_URL)
		result = process.env.OPENSHIFT_MONGODB_DB_URL + 'test1';

	return result;
}

module.exports.url = url;