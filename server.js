/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>


// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)


var passportSecret = require('./config/passport');
var passport = require('passport');

var database = require('./config/database');


var port     = process.env.OPENSHIFT_NODEJS_PORT || 3000;                // set the port
var ipadress = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';          // set server ip

// load the config
console.log("db url: " + database.url());
mongoose.connect(database.url());     // connect to mongoDB database


app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

//app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());




// listen (start app with node server.js) ======================================
app.listen(port, ipadress);
console.log("App listening on address: "  + ipadress + " port " + port);
module.exports = app;

// required for passport
//app.use(session(passportSecret)); // session secret
require('./config/passport')(passport); // pass passport for configuration

// routes ======================================================================
require('./server/routes/api.js')(app);
require('./server/routes/auth.js')(app, passport);



