/// <reference path="../../typings/node/node.d.ts"/>
/// <reference path="../../typings/express/express.d.ts"/>
/// <reference path="../../typings/passport/passport.d.ts"/>
/// <reference path="../../typings/passport-strategy/passport-strategy.d.ts"/>
/// <reference path="../../typings/passport-local/passport-local.d.ts"/>

'use strict';

var path = require('path');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var jwtSecret = require("../../config/jwt").secret;


module.exports = function(app, passport) {


// normal routes ===============================================================

	// LOGOUT ==============================
	app.post('/logout', function(req, res) {
		req.logout();
		res.json({ redirect: '/logout' });
	});

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

	// locally --------------------------------
		// LOGIN ===============================
/*
    	app.get('/login', function(req, res) {
        	res.sendFile(path.join(__dirname, '../../public/views/', 'login.html')); // load the single view file (angular will handle the page changes on the front-end)
    	});
*/
		// process the login form
		app.post('/login', function(req, res, next) {
		    if (!req.body.email || !req.body.password) {
		        return res.json({ error: 'Email and Password required' });
		    }
		    passport.authenticate('local-login', function(err, user, info) {
		        if (err) { 
		            return res.json(err);
		        }
		        if (user.error) {
		            return res.json({ error: user.error });
		        }
		        req.logIn(user, function(err) {
		            if (err) {
		                return res.json(err);
		            }

   		           	console.log(user + " logged in");
					
   		           	var token = jwt.sign(req.user, jwtSecret, { expiresInMinutes: 60*5 });
		            return res.status(200).json(token);
					

		        });
		    })(req, res);
		});

		// SIGNUP =================================

		// process the signup form
		app.post('/signup', function(req, res, next) {
		    if (!req.body.email || !req.body.password) {
		        return res.json({ error: 'Email and Password required' });
		    }
		    passport.authenticate('local-signup', function(err, user, info) {
		        if (err) { 
		            return res.json(err);
		        }
		        if (user.error) {
		            return res.json({ error: user.error });
		        }
		        req.logIn(user, function(err) {
		            if (err) {
		                return res.json(err);
		            }

   		           	console.log(user + " signed up and logged in");
					
   		           	var token = jwt.sign(req.user, jwtSecret, { expiresInMinutes: 60*5 });
		            return res.status(200).json(token);
		        });
		    })(req, res);
		});


	// facebook -------------------------------

		// send to facebook to do the authentication
		app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

		// handle the callback after facebook has authenticated the user
		app.get('/auth/facebook/callback',
			passport.authenticate('facebook', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));

	// twitter --------------------------------

		// send to twitter to do the authentication
		app.get('/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));

		// handle the callback after twitter has authenticated the user
		app.get('/auth/twitter/callback',
			passport.authenticate('twitter', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));


	// google ---------------------------------

		// send to google to do the authentication
		app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

		// the callback after google has authenticated the user
		app.get('/auth/google/callback',
			passport.authenticate('google', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

	// locally --------------------------------
		app.post('/connect/local', function(req, res, next) {
		    if (!req.body.email || !req.body.password) {
		        return res.json({ error: 'Email and Password required' });
		    }
		    passport.authenticate('local-signup', function(err, user, info) {
		        if (err) { 
		            return res.json(err);
		        }
		        if (user.error) {
		            return res.json({ error: user.error });
		        }
		        req.logIn(user, function(err) {
		            if (err) {
		                return res.json(err);
		            }
		            return res.json({ redirect: '/profile' });
		        });
		    })(req, res);
		});

	// facebook -------------------------------

		// send to facebook to do the authentication
		app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

		// handle the callback after facebook has authorized the user
		app.get('/connect/facebook/callback',
			passport.authorize('facebook', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));

	// twitter --------------------------------

		// send to twitter to do the authentication
		app.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

		// handle the callback after twitter has authorized the user
		app.get('/connect/twitter/callback',
			passport.authorize('twitter', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));


	// google ---------------------------------

		// send to google to do the authentication
		app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

		// the callback after google has authorized the user
		app.get('/connect/google/callback',
			passport.authorize('google', {
				successRedirect : '/profile',
				failureRedirect : '/'
			}));

    app.get('/api/userData', isLoggedInAjax, function(req, res) {
        return res.json(req.user);
    });


    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendFile(path.join(__dirname + '../../../public/views/' , 'index.html')); // load the single view file (angular will handle the page changes on the front-end)
    });

};

// route middleware to ensure user is logged in - ajax get
function isLoggedInAjax(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.json( { redirect: '/login' } );
    } else {
        next();
    }
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated())
		return next();

	res.redirect('/');
}