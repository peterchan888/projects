'use strict';

// load the todo model
var Product = require('../models/product');

// expose the routes to our app with module.exports
module.exports = function(app) {

    // api ---------------------------------------------------------------------
    // get all products
    app.get('/api/products', function(req, res) {

         // use mongoose to get all products in the database
        Product.find(function(err, products) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err);

            res.json(products); // return all products in JSON format
        });

    });

    // create product and send back all products after creation
    app.post('/api/products', function(req, res) {

       // create a product, information comes from AJAX request from Angular
        Product.create({
            name : req.body.text,
            available : true
        }, function(err, product) {
            if (err)
                res.send(err);

            // get and return all the products after you create another
            Product.find(function(err, products) {
                if (err)
                    res.send(err);
                res.json(products);
            });
        });


    });

    // delete a product
    app.delete('/api/product/:product_id', function(req, res) {

        Product.remove({
            _id : req.params.product_id
        }, function(err, product) {
            if (err)
                res.send(err);

            // get and return all the products after you create another
            Product.find(function(err, products) {
                if (err)
                    res.send(err);
                res.json(products);
            });
        });

    });

};