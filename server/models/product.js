var mongoose = require('mongoose');

module.exports = mongoose.model('Product', 
{
    name 			: String,
    description 	: String,
    price			: Number,
    pics		 	: [{ filename : String}],
    tags 			: [{ tag : String}],
    stockCount		: Number,    
    available 		: Boolean
});
